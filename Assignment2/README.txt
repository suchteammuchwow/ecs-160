README

Such Team Much Wow:
Dhawal Chandra
Sherrie Shan
Jaclyn Tseng
Watson Wong

All cases from Assignment 1 are listed with minor changes to a few of them.

All HTML files require stylesheet.css to work properly.

These were all created using CHROME, so this is the preferred browser for the best viewing experience.
	Some test cases will NOT work well with Internet Explorer or Firefox.

All test cases have minor interactive components (e.g. clicking a button to progress to the next screen, hyperlinking to other use cases). You can also browse through screens by scrolling.